#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>

#include "RyanLogger.h"
#include "EntityStorage.h"

#define SCREEN_WIDTH  1024
#define SCREEN_HEIGHT 768

#define MAX_BULLETS 512
#define MAX_MECHS   100


struct player_commands
{
   bool up;
   bool down;
   bool left;
   bool right;
   bool fire;
   float angle_rad;
};

struct commands
{
   float horizontalPercentage;
   float verticalPercentage;
   bool fire;
   float angle_rad;
};

static inline
float ComputeLength(float x, float y)
{
   return (float)sqrt((x * x) + (y * y));
}

static inline
void CommandsNormalize(struct commands * commands)
{
   float length = ComputeLength(commands->horizontalPercentage, 
                                commands->verticalPercentage);
   if(length > 1.0f)
   {
      commands->verticalPercentage   /= length;
      commands->horizontalPercentage /= length;
   }
}

static inline
void CommandsInitFromPlayer(struct commands * commands, 
                            const struct player_commands * playerCommands)
{
   commands->angle_rad = playerCommands->angle_rad;
   commands->fire      = playerCommands->fire;
   
   if(playerCommands->up)
   {
      commands->verticalPercentage = 1.0f;
   }
   else if(playerCommands->down)
   {
      commands->verticalPercentage = -1.0f;
   }
   else
   {
      commands->verticalPercentage = 0;
   }
   
   if(playerCommands->right)
   {
      commands->horizontalPercentage = 1.0f;
   }
   else if(playerCommands->left)
   {
      commands->horizontalPercentage = -1.0f;
   }
   else
   {
      commands->horizontalPercentage = 0;
   }

   CommandsNormalize(commands);
}



struct draw_type
{
   SDL_Texture * text;
   int width;
   int height;
   float centerX;
   float centerY;
   int maxZoom;
   float angleOffset_rad;
};

struct draw
{
   const struct draw_type * type;
   float x;
   float y;
   float angle_rad;
};

struct camera
{
   int x;
   int y;
   int zoom;
};

static float EarX = 0;
static float EarY = 0;

static inline
Mix_Chunk * SDLLoadSoundEffect(const char * soundFilename)
{
   Mix_Chunk * sound = Mix_LoadWAV(soundFilename);
   rlmAssert(sound != NULL, "Failed to load sound file \"%s\" because: %s", soundFilename, Mix_GetError());
   rlmLogInfo("Loaded Sound: %s", soundFilename);
   return sound;
}

static inline
void AudioSetEarAt(float x, float y)
{
   EarX = x;
   EarY = y;
}

static inline 
void AudioPlaySoundAt(int channel, Mix_Chunk * chunk, int loops, 
                      float x, float y)
{
   channel = Mix_PlayChannel(channel, chunk, loops);
   if(channel == -1)
   {
      rlmLogWarning("Failed to play sound 0x%p becuase: %s", chunk, Mix_GetError());
   }
   else
   {
      float dx = x - EarX;
      float dy = y - EarY;
      float distance = ComputeLength(dx, dy);
      float volume = 1 - (distance / 2000);
      
      if(volume < 0)
      {
         volume = 0;
      }
      
      Mix_Volume(channel, MIX_MAX_VOLUME * volume);
   }
}

struct bullet_type;

struct mech_type
{
   const struct draw_type * drawType;
   const struct bullet_type * bulletType;
   float hitRadius;
   int maxHealth;
   float speed;
   float rotationSpeed_radPerSec;
   Mix_Chunk * destructionSound;
};

struct mech
{
   const struct mech_type * type;
   float x;
   float y;
   float velX;
   float velY;
   float angle_rad;
   unsigned int health;
   unsigned int refCount;
   bool destroyed;
   struct commands commands;
   int team;
   
   // Problably not part of a mech
   float coolDownTimer;
};


esCreateType(mech, struct mech, MAX_MECHS)


static inline
void MechInit(struct mech * mech, const struct mech_type * type, 
              int team, float x, float y, float angle_rad)
{
   mech->type          = type;
   mech->x             = x;
   mech->y             = y;
   mech->angle_rad     = angle_rad;
   mech->health        = type->maxHealth;
   mech->coolDownTimer = 0;
   mech->refCount      = 1;
   mech->destroyed     = false;
   mech->team          = team;
   
   mech->commands.horizontalPercentage = 0;
   mech->commands.verticalPercentage   = 0;
   mech->commands.fire                 = false;
   mech->commands.angle_rad            = angle_rad;
}

static inline
void MechDammage(struct mech * mech, int dammage)
{
   if(dammage < 0)
   {
      mech->health +=  -dammage;
   }
   else if((unsigned int)dammage >= mech->health)
   {
      mech->health = 0;
   }
   else
   {
      mech->health -= dammage;
   }
   //rlmLogDebug("Mech Health: %u", mech->health);
}

static inline
float MechDistanceToMech(const struct mech * a, const struct mech * b)
{
   float dx = b->x - a->x;
   float dy = b->y - a->y;
   return ComputeLength(dx, dy);
}

static inline
void MechTake(const struct mech * mech)
{
   // In thery change the refCount does not change
   // the mech data. I wonder if there is a better way to do this
   struct mech * writableMech = (struct mech*)mech;
   writableMech->refCount ++;
}

static inline
void MechRelease(const struct mech * mech)
{
   // In thery change the refCount does not change
   // the mech data. I wonder if there is a better way to do this
   struct mech * writableMech = (struct mech*)mech;

   if(writableMech->refCount > 0)
   {
      writableMech->refCount -- ;
   }
}

struct entity_storage_bullet;
static inline
void UpdateMech(float dt_seconds, struct mech * mech, 
                struct entity_storage_bullet * bulletList);

static inline
void MechListUpdate(struct entity_storage_mech * storage, float dt_seconds, struct entity_storage_bullet * bulletList)
{
   struct mech * mech = NULL;
   while(esHasNext_mech(storage, &mech))
   {
      if(mech->health == 0 && !mech->destroyed)
      {
         mech->destroyed = true;
         AudioPlaySoundAt(-1, mech->type->destructionSound, 0, 
                          mech->x, mech->y);
         MechRelease(mech);
      }
      
      if(mech->refCount == 0)
      {
         esFree_mech(storage, mech);
      }
      else if(!mech->destroyed)
      {
         UpdateMech(dt_seconds, mech, bulletList);
      }
   }
}

struct bullet_type
{
   const struct draw_type * drawType;
   float speed;
   int dammage;
   float timeout;
   
   // Problably more related to a gun than a bullet
   float spread_rad;
   float fireDelay_second;
   Mix_Chunk * fireSound;
};

struct bullet
{
   const struct bullet_type * type;
   float x;
   float y;
   float angle_rad;
   float velX;
   float velY;
   float timer;
   const struct mech * firedFrom;
};

static inline
float RandomizeAngle(float angle, float range)
{
   float start = angle  - range / 2;

   float value = rand() / (float)RAND_MAX;
   return start + value * range;
}

static inline
void BulletInit(struct bullet * bullet, const struct bullet_type * type,
                const struct mech * firedFrom, 
                float x, float y, float angle_rad)
{
   bullet->type      = type;
   bullet->firedFrom = firedFrom;
   bullet->angle_rad = angle_rad;
   bullet->x         = x;
   bullet->y         = y;
   
   // Computations
   bullet->timer = bullet->type->timeout;
   bullet->velX  = bullet->firedFrom->velX + 
                   cos(bullet->angle_rad) * bullet->type->speed ;
   bullet->velY  = bullet->firedFrom->velY + 
                   sin(bullet->angle_rad) * bullet->type->speed;
   
   MechTake(bullet->firedFrom);
}

esCreateType(bullet, struct bullet, MAX_BULLETS)

static inline
void BulletUpdate(struct bullet * bullet, 
                  struct entity_storage_bullet * bulletList, 
                  float dt_seconds)
{
   bullet->x += bullet->velX * dt_seconds;
   bullet->y += bullet->velY * dt_seconds;
   if(bullet->timer <= dt_seconds)
   {
      bullet->timer = 0;
      MechRelease(bullet->firedFrom);
      esFree_bullet(bulletList, bullet);
   }
   else
   {
      bullet->timer -= dt_seconds;
   }
}

static inline
void BulletListUpdate(struct entity_storage_bullet * storage, float dt_seconds)
{
   struct bullet * bullet = NULL;
   while(esHasNext_bullet(storage, &bullet))
   {
      BulletUpdate(bullet, storage, dt_seconds);
   }
}

struct ai_data
{
   struct mech * controlled;
   const struct mech * target;   
   float rangeMin;
   float rangeMax;
   float retargetTimer_second;
};

esCreateType(ai, struct ai_data, MAX_MECHS)

struct targeting_data
{
   const struct mech * src;
   const struct mech * target;
   float dx;
   float dy;
   float dirX;
   float dirY;
   float dVelX;
   float dVelY;
   float distance;
   float angle_rad;
};

static inline
void TargetingDataInit(struct targeting_data * data, const struct mech * src,
                                                     const struct mech * target)
{
   data->src    = src;
   data->target = target;
   
   // Computations
   data->dx = data->target->x - data->src->x;
   data->dy = data->target->y - data->src->y;
   
   data->dVelX = data->target->velX - data->src->velX;
   data->dVelY = data->target->velY - data->src->velY;
   
   data->distance  = MechDistanceToMech(data->src, data->target);
   data->angle_rad = atan2(data->dy, data->dx);
   
   if(data->distance > 0.001)
   {
      data->dirX = data->dx / data->distance;
      data->dirY = data->dy / data->distance;
   }
   else
   {
      data->dirX = 0;
      data->dirY = 0;
   }
}

struct lead_data
{
   const struct targeting_data * targetingData;
   float projectileSpeed;
   float projectileTravelTime;
   float dx;
   float dy;
   float angle_rad;
};

static inline
void LeadDataInit(struct lead_data * data, 
                  const struct targeting_data * targetingData,
                  float projectileSpeed)
{
   data->targetingData   = targetingData;
   data->projectileSpeed = projectileSpeed;
   
   // Computations
   if(data->projectileSpeed > 0.001)
   {
      data->projectileTravelTime = data->targetingData->distance / 
                                   data->projectileSpeed;
   }
   else
   {
      data->projectileTravelTime = 0;
   }
   
   data->dx = data->targetingData->dx + 
              data->targetingData->dVelX * data->projectileTravelTime;
   data->dy = data->targetingData->dy + 
              data->targetingData->dVelY * data->projectileTravelTime;
   
   data->angle_rad = atan2(data->dy, data->dx);
}

static inline
struct mech * FindClosestHostileMech(const struct mech * mech, 
                                     const struct entity_storage_mech * mechList)
{
   struct mech * aMech   = NULL;
   struct mech * closest = NULL;
   float distance;
   while(esHasNext_mech(mechList, &aMech))
   {
      if(mech->team != aMech->team &&
         !aMech->destroyed)
      {
         float aDistance = MechDistanceToMech(mech, aMech);
         if(closest == NULL || aDistance < distance)
         {
            closest = aMech;
            distance = aDistance;
         }
      }
   }
   return closest;
}

static inline
float ToDegree(float angle_rad)
{
   return angle_rad * 180 / 3.14;
}

static inline
float ToRadians(float angle_deg)
{
   return angle_deg * 3.14 / 180;
}

static inline
void DrawTerrainImageAt(SDL_Renderer * rend, const struct camera * cam, 
                        SDL_Texture * texture, int x, int y, int width, int height)
{
   SDL_Rect dest;
   dest.x = ( x - cam->x) / cam->zoom;
   dest.y = (-y + cam->y) / cam->zoom;
   dest.w = (width  / cam->zoom) + 1;
   dest.h = (height / cam->zoom) + 1;
   SDL_RenderCopy(rend, texture, NULL, &dest);
}

static inline
void DrawDraw(SDL_Renderer * rend, struct camera * cam, struct draw * draw)
{
   SDL_Rect dest;
   SDL_Point center;
   float rotation;
   float cxoz, cyoz;
   int zoom;
   if(cam->zoom > draw->type->maxZoom)
   {
      zoom = draw->type->maxZoom;
   }
   else
   {
      zoom = cam->zoom;
   }

   cxoz = draw->type->centerX / zoom;
   cyoz = draw->type->centerY / zoom;
   
   
   center.x = cxoz;
   center.y = cyoz;
   dest.x = ( draw->x - cam->x) / cam->zoom - cxoz;
   dest.y = (-draw->y + cam->y) / cam->zoom - cyoz;
   dest.w = (draw->type->width  + (zoom / 2)) / zoom;
   dest.h = (draw->type->height + (zoom / 2)) / zoom;
   rotation = -ToDegree(draw->angle_rad - draw->type->angleOffset_rad); 
   SDL_RenderCopyEx(rend, draw->type->text, NULL, &dest, rotation, &center, SDL_FLIP_NONE);
}

static inline
SDL_Texture * SDLLoadImage(SDL_Renderer * rend, const char * imageFilename)
{
   SDL_Surface * surf;
   SDL_Texture * text;
   surf =  IMG_Load(imageFilename);
   rlmAssert(surf != NULL, "Failed to load Image \"%s\" becuase: %s", imageFilename, IMG_GetError());
   text = SDL_CreateTextureFromSurface(rend, surf);
   rlmAssert(text != NULL, "Failed to convert surface to texture for image \"%s\" Reason: %s", imageFilename, SDL_GetError());

   SDL_FreeSurface(surf);
   rlmLogInfo("Loaded Image: %s", imageFilename);
   return text;
}


static inline
void LimmitFrames(Uint32 * milliseconds, Uint32 limmitTo_ms)
{
   Uint32 now_ms = SDL_GetTicks();
   Uint32 diff = now_ms - *milliseconds;
   (*milliseconds) = now_ms;
   if(diff < limmitTo_ms)
   {
      SDL_Delay(limmitTo_ms - diff);
   }
}

static inline
float GetDeltaTime_seconds(Uint32 * milliseconds)
{
   Uint32 now_ms = SDL_GetTicks();
   Uint32 diff = now_ms - *milliseconds;
   (*milliseconds) = now_ms;
   return diff / 1000.0f;
}

static inline
void ScreenToWorld(int screenX, int screenY, const struct camera * cam, int * worldX, int * worldY)
{
   screenX *= cam->zoom;
   screenY *= cam->zoom;
   screenX =  screenX + cam->x;
   screenY = -screenY + cam->y;
   
   if(worldX != NULL)
   {
      (*worldX) = screenX;
   }
   if(worldY != NULL)
   {
      (*worldY) = screenY;
   }
};

static inline
float GetAngleOfMouseFromCamera_Rad(int mouseX, int mouseY, float entityX, float entityY, const struct camera * cam)
{
   float dx, dy;
   int worldX, worldY;
   
   ScreenToWorld(mouseX, mouseY, cam, &worldX, &worldY);
   
   dx = worldX - entityX;
   dy = worldY - entityY;
   return (float)atan2(dy, dx);
}



static inline
void ApplyDeadzone(float * x, float * y, float deadzone)
{
   float length = ComputeLength(*x, *y);
   
   if(length <= deadzone)
   {
      (*x) = 0;
      (*y) = 0;
   }
   else
   {
      float ratio = (length - deadzone) / length;
      (*x) *= ratio;
      (*y) *= ratio;
   }
}

static inline
void UpdateCameraBasedOnMouse(struct camera * cam, float entityX, float entityY,
                              int mouseX, int mouseY, 
                              int screenWidth, int screenHeight)
{
   // Center Camera on Meck
   const float deadzone = 0.5;
   const int halfZoomedWidth  = (screenWidth  * cam->zoom) / 2;
   const int halfZoomedHeight = (screenHeight * cam->zoom) / 2;
   float xPercent = mouseX / (float)screenWidth;
   float yPercent = mouseY / (float)screenHeight;
   int deltaX;
   int deltaY;

   xPercent =  xPercent * 2 - 1;
   yPercent = -yPercent * 2 + 1;

   ApplyDeadzone(&xPercent, &yPercent, deadzone);
   

   deltaX = halfZoomedWidth  * xPercent;
   deltaY = halfZoomedHeight * yPercent;

   cam->x = entityX + deltaX - halfZoomedWidth;
   cam->y = entityY + deltaY + halfZoomedHeight;
}

static inline
float AngleNormalize(float angle_rad)
{
   float out = fmod(angle_rad + 3.14, 2 * 3.14);
   if(out < 0)
   {
      out += 2 * 3.14;
   }
   return out - 3.14;
}

static inline
float AngleSlew(float currentAngle_rad, float targetAngle_rad, float deltaAngle_rad)
{
   float diff = AngleNormalize(targetAngle_rad - currentAngle_rad);

   if(diff >= 0)
   {
      if(diff < deltaAngle_rad)
      {
         return targetAngle_rad;
      }
      else
      {
         return currentAngle_rad + deltaAngle_rad;
      }
   }
   else
   {
      if(-diff < deltaAngle_rad)
      {
         return targetAngle_rad;
      }
      else
      {
         return currentAngle_rad - deltaAngle_rad;
      }
   }
}

static inline
void UpdateMech(float dt_seconds, struct mech * mech, 
                struct entity_storage_bullet * bulletList)
{
   float percent_x = 0;
   float percent_y = 0;
   float length;
   const struct commands * commands = &mech->commands;
   const struct mech_type * type = mech->type;
   
   //dt_seconds = 17/1000.0f;
   
   // Compute Velocity
   mech->velX = commands->horizontalPercentage * type->speed;
   mech->velY = commands->verticalPercentage   * type->speed;
   
   // Maybe this should be in a diffrent function?
   mech->x += mech->velX * dt_seconds;
   mech->y += mech->velY * dt_seconds;
   
   mech->angle_rad = AngleSlew(mech->angle_rad, commands->angle_rad, 
                               type->rotationSpeed_radPerSec * dt_seconds);

   if(mech->coolDownTimer <= dt_seconds)
   {
      mech->coolDownTimer = 0;
      if(commands->fire)
      {
         struct bullet * bullet = esTake_bullet(bulletList);
         if(bullet != NULL)
         {
            float bulletAngle_rad = RandomizeAngle(mech->angle_rad, 
                                             type->bulletType->spread_rad);
            BulletInit(bullet, 
                       type->bulletType, mech,
                       mech->x, mech->y, 
                       bulletAngle_rad);
            AudioPlaySoundAt(-1, type->bulletType->fireSound, 0,
                             bullet->x, bullet->y);
         }
         else
         {
            rlmLogError("Failed to get next bullet when fireing");
         }
         mech->coolDownTimer = type->bulletType->fireDelay_second;
      }
   }
   else
   {
      mech->coolDownTimer -= dt_seconds;
   }

}

static inline
bool CheckOverlap(const struct mech * mech, const struct bullet * bullet)
{
   float hitRadius = mech->type->hitRadius;
   float dx = mech->x - bullet->x;
   float dy = mech->y - bullet->y;
   float distance = ComputeLength(dx, dy);
   return distance <= hitRadius;
}

static inline 
void CollisionDetection(struct entity_storage_mech * mechList, 
                        struct entity_storage_bullet * bulletList)
{
   struct mech * mech = NULL;
   while(esHasNext_mech(mechList, &mech))
   {
      struct bullet * bullet = NULL;
      while((bullet = esNext_bullet(bulletList, bullet)) != NULL)
      {
         if(bullet->firedFrom != mech && !mech->destroyed && 
            CheckOverlap(mech, bullet))
         {
            esFree_bullet(bulletList, bullet);
            MechDammage(mech, 5);
         }
      }
   }
}

static inline 
void DrawTexture(SDL_Renderer * rend, SDL_Texture * text, int x, int y, int width, int height)
{
   SDL_Rect dest;
   dest.x = x;
   dest.y = y;
   dest.w = width;
   dest.h = height;
   SDL_RenderCopy(rend, text, NULL, &dest);
}

static inline
void AIUpdate(struct ai_data * ai, const struct entity_storage_mech * mechList, 
              float dt_seconds)
{
   struct commands * aiCom = &ai->controlled->commands;
   // Give up target if dead
   if(ai->target != NULL)
   {
      if(ai->target->destroyed)
      {
         MechRelease(ai->target);
         ai->target = NULL;
      }
   }
   
   if(ai->retargetTimer_second < dt_seconds)
   {
      struct mech * closestHostile;

      ai->retargetTimer_second = 1.0f;
      closestHostile = FindClosestHostileMech(ai->controlled, 
                                              mechList);
      if(closestHostile != NULL && 
         closestHostile != ai->target)
      {
         if(ai->target != NULL)
         {
            MechRelease(ai->target);
         }
         ai->target = closestHostile;
         MechTake(ai->target);
      }
      
   }
   else
   {
      ai->retargetTimer_second -= dt_seconds;
   }
   
   // Figure distance and angle to target
   if(ai->target != NULL)
   {
      struct targeting_data targetingData;
      struct lead_data leadData;
      
      TargetingDataInit(&targetingData, ai->controlled, ai->target);
      LeadDataInit(&leadData, &targetingData, 
                   ai->controlled->type->bulletType->speed);
      
      ai->rangeMax = 1000;
      ai->rangeMin = 500;
      // If in range fire at target
      aiCom->angle_rad = leadData.angle_rad;
      aiCom->fire = targetingData.distance < ai->rangeMax;
      if(targetingData.distance > ai->rangeMax)
      {
         aiCom->horizontalPercentage = targetingData.dirX;
         aiCom->verticalPercentage   = targetingData.dirY;
      }
      else if(targetingData.distance < ai->rangeMin)
      {
         aiCom->horizontalPercentage = -targetingData.dirX;
         aiCom->verticalPercentage   = -targetingData.dirY;
      }
      else
      {
         aiCom->horizontalPercentage = 0;
         aiCom->verticalPercentage   = 0;
      }

      CommandsNormalize(aiCom);
   }
   else
   {
      aiCom->fire = false;
      aiCom->horizontalPercentage = 0;
      aiCom->verticalPercentage   = 0;
   }
}

static inline
void DrawMechHealthAndTeam(SDL_Renderer * rend, const struct mech * mech, 
                           const struct camera * cam, int playerTeam,
                           SDL_Texture * healthTexture,
                           SDL_Texture * teamTexture)
{
   SDL_Rect src, dest;
   float screenX, screenY;
   float healthPercent;
   float offset;
   // Get Screen Coodinates of Mech
   screenX =   mech->x - cam->x;
   screenY = -(mech->y - cam->y);
   
   screenX /= cam->zoom;
   screenY /= cam->zoom;
   
   // Offset by distance oposite of mech facing
   offset = mech->type->drawType->height / (2 * cam->zoom) + 25;
   screenX += cos(mech->angle_rad + 3.14f) * offset;
   screenY -= sin(mech->angle_rad + 3.14f) * offset;
   
   dest.x = screenX - 20;
   dest.y = screenY - 16;
   
   // Compute Health Percentage
   healthPercent = mech->health / (float)mech->type->maxHealth;
   
   // Fit percents into colors
   
   if(healthPercent > 0.9f)
   {
      src.x = 0; // green
   }
   else if(healthPercent > 0.4f)
   {
      src.x = 32; // yellow
   }
   else
   {
      src.x = 64; // red
   }
   
   
   src.y = 0;
   
   dest.w = src.w = 32;
   dest.h = src.h = 32;
   
   SDL_SetTextureAlphaMod(healthTexture, 100);
   SDL_SetTextureAlphaMod(teamTexture, 100);

   
   SDL_RenderCopy(rend, healthTexture, &src, &dest);
   
   
   
   dest.x = screenX + 12;
   dest.y = screenY - 16;
   if(mech->team == playerTeam)
   {
      src.x = 0; // green
   }
   else
   {
      src.x = 8; // red
   }
   
   src.y = 0;
   dest.w = src.w = 8;
   dest.h = src.h = 8;
   SDL_RenderCopy(rend, teamTexture, &src, &dest);
}


static inline
struct mech * AddMechToFieldWithAI(struct entity_storage_mech * mechList,
                                   struct entity_storage_ai * aiList,
                                   const struct mech_type * type,
                                   int team,
                                   float x, float y,
                                   float angle_rad)
{
   struct mech * other;
   struct ai_data * ai;
   
   other = esTake_mech(mechList);
   rlmAssert(other != NULL, "Failed to get other mech memory");
   MechInit(other, type, team, x, y, angle_rad);

   ai = esTake_ai(aiList);
   rlmAssert(ai != NULL, "Failed to get AI memory");
   ai->target = NULL;
   ai->controlled = other;
   ai->retargetTimer_second = 0;
   MechTake(ai->controlled);
   return other;
}

static inline 
struct mech * SetupArena(struct entity_storage_mech * mechList,
                         struct entity_storage_bullet * bulletList,
                         struct entity_storage_ai * aiList,
                         const struct mech_type * mechTypeA, 
                         const struct mech_type * mechTypeB)
{
   struct mech * player;
   
   esClear_mech(mechList);
   esClear_bullet(bulletList);
   esClear_ai(aiList);

   // TEAM 0
   player = esTake_mech(mechList);
   rlmAssert(player != NULL, "Failed to get player mech memory");
   MechInit(player, mechTypeA, 0, 0, 0, 0);
   MechTake(player); // Don't automagicaly delete the player mech
   
   AddMechToFieldWithAI(mechList, aiList, mechTypeB, 0, 0,   300, 0);
   AddMechToFieldWithAI(mechList, aiList, mechTypeB, 0, 200, 500, 0);
   
   // TEAM 1
   AddMechToFieldWithAI(mechList, aiList, mechTypeB, 1, 1500, -1500, ToRadians(135));
   AddMechToFieldWithAI(mechList, aiList, mechTypeA, 1, 1500, -1700, ToRadians(135));
   
   AddMechToFieldWithAI(mechList, aiList, mechTypeB, 1, 1500, -3000, ToRadians(135));
   AddMechToFieldWithAI(mechList, aiList, mechTypeB, 1, 2000, -3000, ToRadians(135));

   return player;
}

int main(int argc, char * args[])
{
   SDL_Window * window;
   SDL_Renderer * renderer;
   SDL_Event event;
   bool running;
   Uint32 limmit_ms;
   Uint32 timer_ms;
   int mouseX = SCREEN_WIDTH  / 2; 
   int mouseY = SCREEN_HEIGHT / 2;
   
   struct draw_type drawTypeMechA, drawTypeMechB, drawTypeBullet;
   struct mech_type mechTypeA, mechTypeB;
   struct bullet_type bulletTypeMachineGun;
   struct mech * player;
   struct entity_storage_bullet bulletList;
   struct entity_storage_mech   mechList;
   struct entity_storage_ai     aiList;
   struct camera cam;
   struct player_commands commands;
   SDL_Texture * grass;
   SDL_Texture * crosshair;
   SDL_Texture * healthTexture;
   SDL_Texture * teamTexture;
   Mix_Chunk   * gunShotSound;
   Mix_Chunk   * explosionSound;  

   rlmAssert(SDL_Init(SDL_INIT_EVERYTHING) == 0, "Couldn't Initialize SDL: %s", SDL_GetError());
   rlmLogInfo("SDL Initialized");
   rlmAssert((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) == IMG_INIT_PNG, "Couldn't Initialize SDL_Image: %s", IMG_GetError());
   rlmLogInfo("SDL_Image Initialized");
   rlmAssert(Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 1024) == 0, "Couldn't Initialize SDL_Mixer: %s", Mix_GetError());
   rlmLogInfo("SDL_Mixer Initialized");
   Mix_AllocateChannels(128);
   
   window = SDL_CreateWindow("Open Pixel Shooter", SDL_WINDOWPOS_UNDEFINED,
                                                   SDL_WINDOWPOS_UNDEFINED,
                                                   SCREEN_WIDTH, SCREEN_HEIGHT, 
                                                   0);
   rlmAssert(window != NULL, "SDL Failed to open window: %s", SDL_GetError());
   rlmLogInfo("SDL Window Created");
   
   renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
   rlmAssert(renderer != NULL, "SDL Failed to create Renderer: %s", SDL_GetError());
   rlmLogInfo("SDL Renderer Created");
   
   esInit_mech(&mechList);
   esInit_bullet(&bulletList);
   esInit_ai(&aiList);
   
   grass          = SDLLoadImage(renderer, "assets/grass.png");
   crosshair      = SDLLoadImage(renderer, "assets/crosshair.png");
   healthTexture  = SDLLoadImage(renderer, "assets/health.png");
   teamTexture    = SDLLoadImage(renderer, "assets/team.png");
   gunShotSound   = SDLLoadSoundEffect("assets/bang_10.ogg");
   explosionSound = SDLLoadSoundEffect("assets/explode.ogg");

   // Draw Types
   drawTypeBullet.text = SDLLoadImage(renderer, "assets/bullet.png");
   drawTypeBullet.width  = 13;
   drawTypeBullet.height = 13;
   drawTypeBullet.centerX = drawTypeBullet.width  / 2.0f;
   drawTypeBullet.centerY = drawTypeBullet.height / 2.0f;
   drawTypeBullet.angleOffset_rad = 3.14 / 2;
   drawTypeBullet.maxZoom = 1;

   drawTypeMechA.text = SDLLoadImage(renderer, "assets/Mech1A.png");
   drawTypeMechA.width = 165;
   drawTypeMechA.height = 97;
   drawTypeMechA.centerX = drawTypeMechA.width  / 2.0f;
   drawTypeMechA.centerY = drawTypeMechA.height / 2.0f;
   drawTypeMechA.angleOffset_rad = 3.14 / 2;
   drawTypeMechA.maxZoom = 6;
      
   drawTypeMechB.text = SDLLoadImage(renderer, "assets/Mech3.png");
   drawTypeMechB.width = 118;
   drawTypeMechB.height = 103;
   drawTypeMechB.centerX = drawTypeMechB.width  / 2.0f;
   drawTypeMechB.centerY = drawTypeMechB.height / 2.0f;
   drawTypeMechB.angleOffset_rad = 3.14 / 2;
   drawTypeMechB.maxZoom = 6;

   // Bullet Types
   bulletTypeMachineGun.drawType         = &drawTypeBullet;
   bulletTypeMachineGun.speed            = 1000.0f;
   bulletTypeMachineGun.timeout          = 4.0f;
   bulletTypeMachineGun.dammage          = 5;
   bulletTypeMachineGun.spread_rad       = ToRadians(6);
   bulletTypeMachineGun.fireDelay_second = 0.1f;
   bulletTypeMachineGun.fireSound        = gunShotSound;

   // Mech Types
   mechTypeA.drawType = &drawTypeMechA;
   mechTypeA.hitRadius = drawTypeMechA.height / 2.0f;
   mechTypeA.maxHealth = 100;
   mechTypeA.speed = 300;
   mechTypeA.rotationSpeed_radPerSec = ToRadians(120);
   mechTypeA.bulletType = &bulletTypeMachineGun;
   mechTypeA.destructionSound = explosionSound;
   
   mechTypeB.drawType = &drawTypeMechB;
   mechTypeB.hitRadius = drawTypeMechB.height / 2.0f;
   mechTypeB.maxHealth = 50;
   mechTypeB.speed = 200;
   mechTypeB.rotationSpeed_radPerSec = ToRadians(120);
   mechTypeB.bulletType = &bulletTypeMachineGun;
   mechTypeB.destructionSound = explosionSound;
   
   
   player = SetupArena(&mechList, &bulletList, &aiList,
                       &mechTypeA,
                       &mechTypeB);


   cam.x    = 0;
   cam.y    = 0;
   cam.zoom = 2;
   
   // Hide Cursor
   SDL_ShowCursor(SDL_DISABLE);
   
   rlmLogInfo("Starting Main Loop");
   running = true;
   timer_ms = limmit_ms = SDL_GetTicks();
   while(running)
   {
      float dt_seconds;
      int desiredCameraZoom = cam.zoom;
      struct draw draw;
      
      while(SDL_PollEvent(&event))
      {
         switch(event.type)
         {
         case SDL_QUIT:
            rlmLogInfo("SDL Event: Quit");
            running = false;
            break;
         case SDL_KEYDOWN:
            switch(event.key.keysym.sym)
            {
            case SDLK_w: commands.up    = true; break;
            case SDLK_s: commands.down  = true; break;
            case SDLK_a: commands.left  = true; break;
            case SDLK_d: commands.right = true; break;
            case SDLK_ESCAPE:
               rlmLogInfo("Escape key pressed");
               running = false;
               break;
            case SDLK_r:
               player = SetupArena(&mechList, &bulletList, &aiList,
                                   &mechTypeA,
                                   &mechTypeB);
               break;
            }
            break;
         case SDL_KEYUP:
            switch(event.key.keysym.sym)
            {
            case SDLK_w: commands.up    = false; break;
            case SDLK_s: commands.down  = false; break;
            case SDLK_a: commands.left  = false; break;
            case SDLK_d: commands.right = false; break;
            }
            break;
         case SDL_MOUSEBUTTONDOWN:
            if(event.button.button == SDL_BUTTON_LEFT)
            {
               commands.fire = true;
            }
            break;
         case SDL_MOUSEBUTTONUP:
            if(event.button.button == SDL_BUTTON_LEFT)
            {
               commands.fire = false;
            }
            break;
         case SDL_MOUSEWHEEL:
            if(cam.zoom - event.wheel.y < 1)
            {
               desiredCameraZoom = 1;
            }
            else if(cam.zoom - event.wheel.y > 5)
            {
               desiredCameraZoom = 5;
            }
            else
            {
               desiredCameraZoom = cam.zoom - event.wheel.y;
            }
            break;
         case SDL_MOUSEMOTION:
            mouseX = event.motion.x;
            mouseY = event.motion.y;
            break;
         }
      }

      commands.angle_rad = GetAngleOfMouseFromCamera_Rad(mouseX,
                                                         mouseY,
                                                         player->x,
                                                         player->y,
                                                         &cam);
      // Get Delta Time
      dt_seconds = GetDeltaTime_seconds(&timer_ms);
      
      // Run AIs
      {
         struct ai_data * ai = NULL;
         while(esHasNext_ai(&aiList, &ai))
         {
            // Delete ourselves if out commanded Mech is destroyed
            if(ai->controlled->destroyed)
            {
               if(ai->target != NULL)
               {
                  MechRelease(ai->target);
               }
               MechRelease(ai->controlled);
               esFree_ai(&aiList, ai);
            }
            else
            {
               AIUpdate(ai, &mechList, dt_seconds);
            }
         }
      }
      
      // Convert Player Commands
      CommandsInitFromPlayer(&player->commands, &commands);
      
      // Update Everyone
      MechListUpdate(&mechList, dt_seconds, &bulletList);
      AudioSetEarAt(player->x, player->y);
      BulletListUpdate(&bulletList,  dt_seconds);
      
      CollisionDetection(&mechList, &bulletList);
    
      // Ajust Camera
      cam.zoom = desiredCameraZoom;
      UpdateCameraBasedOnMouse(&cam, player->x, player->y, mouseX, mouseY, 
                               SCREEN_WIDTH, SCREEN_HEIGHT);
      
      SDL_SetRenderDrawColor(renderer, 96, 128, 255, 255);
      SDL_RenderClear(renderer);
      
      // Draw Stuff Here
      
      // Drawing Ground
      {
         int x, y;
         for(y = -10; y < 10; y++)
            for(x = -10; x < 10; x++)
            {
               DrawTerrainImageAt(renderer, &cam, grass, x * 512, y * 512, 512, 512);
            }
      }
      
      // Draw Mechs
      {
         struct mech * mech = NULL;         
         while(esHasNext_mech(&mechList, &mech))
         {
            if(!mech->destroyed)
            {
               draw.type = mech->type->drawType;
               draw.x = mech->x;
               draw.y = mech->y;
               draw.angle_rad = mech->angle_rad;
               DrawDraw(renderer, &cam, &draw);
            }
         }
      } 

      // Draw Bullets
      {
         struct bullet * bullet = NULL;         
         draw.type = &drawTypeBullet;
         while(esHasNext_bullet(&bulletList, &bullet))
         {
            draw.x = bullet->x;
            draw.y = bullet->y;
            draw.angle_rad = bullet->angle_rad;
            DrawDraw(renderer, &cam, &draw);
         }
      } 
      
      // Draw Mech Health and Team indicators
      {
         struct mech * mech = NULL;         
         while(esHasNext_mech(&mechList, &mech))
         {
            if(!mech->destroyed)
            {
               DrawMechHealthAndTeam(renderer, mech, &cam, player->team,
                                     healthTexture, teamTexture);
            }
         }
      } 
      
      // Draw the Curosr
      DrawTexture(renderer, crosshair, mouseX - 16, mouseY - 16, 32, 32);
      
      SDL_RenderPresent(renderer);

      LimmitFrames(&limmit_ms, 17); // limmit to 60 fps
   }
   rlmLogInfo("Exiting Main Loop");
   
   Mix_FreeChunk(gunShotSound);
   Mix_FreeChunk(explosionSound);
   
   SDL_DestroyTexture(grass);
   SDL_DestroyTexture(crosshair);
   SDL_DestroyTexture(healthTexture);
   SDL_DestroyTexture(teamTexture);
   
   SDL_DestroyTexture(drawTypeBullet.text);
   SDL_DestroyTexture(drawTypeMechA.text);
   SDL_DestroyTexture(drawTypeMechB.text);
   
   SDL_DestroyWindow(window);
   rlmLogInfo("SDL Window Destroyed");
   Mix_CloseAudio();
   rlmLogInfo("SDL_Mixer Quit");
   IMG_Quit();
   rlmLogInfo("SDL_Image Quit");
   SDL_Quit();
   rlmLogInfo("SDL Quit");
   
   rlmLogInfo("End of Program");
   return 0;
}
