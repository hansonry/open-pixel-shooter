#ifndef __RYANLOGER_H__
#define __RYANLOGER_H__

enum ryan_logger_level
{
   rlLogLevelNone,
   rlLogLevelTrace,
   rlLogLevelDebug,
   rlLogLevelInfo,
   rlLogLevelWarning,
   rlLogLevelError,
   rlLogLevelFatal
};

void rlSetMinimumLevel(enum ryan_logger_level level);

const char * rlGetLevelText(enum ryan_logger_level level);


#define rlmLog(level, ...) rlLog(__FILE__, __LINE__, (level), __VA_ARGS__)
void rlLog(const char * filename, int line, enum ryan_logger_level level, const char * format, ...);


#define rlmLogTrace(...)   rlLog(__FILE__, __LINE__, rlLogLevelTrace,   __VA_ARGS__)
#define rlmLogDebug(...)   rlLog(__FILE__, __LINE__, rlLogLevelDebug,   __VA_ARGS__)
#define rlmLogInfo(...)    rlLog(__FILE__, __LINE__, rlLogLevelInfo,    __VA_ARGS__)
#define rlmLogWarning(...) rlLog(__FILE__, __LINE__, rlLogLevelWarning, __VA_ARGS__)
#define rlmLogError(...)   rlLog(__FILE__, __LINE__, rlLogLevelError,   __VA_ARGS__)
#define rlmLogFatal(...)   rlLog(__FILE__, __LINE__, rlLogLevelFatal,   __VA_ARGS__)

#define rlmAssert(value, ...) \
if(!(value)) rlLog(__FILE__, __LINE__, rlLogLevelFatal, __VA_ARGS__)

#endif //__RYANLOGER_H__
