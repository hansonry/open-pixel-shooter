#ifndef __ENTITYSTORAGE_H__
#define __ENTITYSTORAGE_H__
#include <stddef.h>

struct entity_storage
{
   size_t entitySize;
   size_t maxCount;
   unsigned char * activeFlags;
   void * entityData;
   size_t nextEntityHint;
   size_t count;
};

void esInit(struct entity_storage * storage, 
            size_t entitySize, 
            size_t maxCount, 
            unsigned char * activeFlags, 
            void * entityData);


void * esTake(struct entity_storage * storage);

void esFree(struct entity_storage * storage, void * ptr);

void * esNext(const struct entity_storage * storage, void * ptr);

void esClear(struct entity_storage * storage);


#define esCreateType(typeName, type, maxCount)                                    \
                                                                                  \
struct entity_storage_ ## typeName                                                \
{                                                                                 \
   struct entity_storage storage;                                                 \
   type entities[maxCount];                                                       \
   unsigned char activeFlags[(maxCount + 7) / 8];                                 \
};                                                                                \
                                                                                  \
static inline                                                                     \
void esInit_ ## typeName (struct entity_storage_ ## typeName  * storage)          \
{                                                                                 \
   esInit(&storage->storage, sizeof(type), maxCount,                              \
          storage->activeFlags, storage->entities);                               \
}                                                                                 \
                                                                                  \
static inline                                                                     \
type * esTake_ ## typeName (struct entity_storage_ ## typeName  * storage)        \
{                                                                                 \
   return esTake(&storage->storage);                                              \
}                                                                                 \
                                                                                  \
static inline                                                                     \
void esFree_ ## typeName (struct entity_storage_ ## typeName  * storage,          \
                          type * ptr)                                             \
{                                                                                 \
   esFree(&storage->storage, ptr);                                                \
}                                                                                 \
                                                                                  \
static inline                                                                     \
type * esNext_ ## typeName (const struct entity_storage_ ## typeName  * storage,  \
                            type * ptr)                                           \
{                                                                                 \
   return esNext(&storage->storage, ptr);                                         \
}                                                                                 \
                                                                                  \
static inline                                                                     \
int esHasNext_ ## typeName (const struct entity_storage_ ## typeName  * storage,  \
                            type ** ptr)                                          \
{                                                                                 \
   (*ptr) = esNext(&storage->storage, (*ptr));                                    \
   return (*ptr) != NULL;                                                         \
}                                                                                 \
                                                                                  \
static inline                                                                     \
void esClear_ ## typeName (struct entity_storage_ ## typeName  * storage)         \
{                                                                                 \
   esClear(&storage->storage);                                                    \
}                                                                                 \
                                                                                  \
static inline                                                                     \
size_t esCount_ ## typeName (const struct entity_storage_ ## typeName  * storage) \
{                                                                                 \
   return storage->storage.count;                                                 \
}

#endif // __ENTITYSTORAGE_H__
