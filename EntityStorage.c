#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "EntityStorage.h"

struct entity_storage_active_flag_ptr
{
   unsigned char * flagByte;
   unsigned char bit;
};

struct entity_storage_iterator
{
   const struct entity_storage * storage;
   struct entity_storage_active_flag_ptr activePtr;
   unsigned char * memory;
   size_t index;
   int traveled;
};

static inline
size_t esGetActiveFlagsSizeFromMaxCount(size_t maxCount)
{
   return (maxCount + 7) / 8;
}

static inline
void esSetActiveFlagPointer(const struct entity_storage * storage,
                            struct entity_storage_active_flag_ptr * ptr,
                            size_t index)
{
   if(index < storage->maxCount)
   {
      ptr->bit      = index % 8;
      ptr->flagByte = &storage->activeFlags[index / 8];
   }
   else
   {
      ptr->bit      = 0;
      ptr->flagByte = NULL;
   }
}

static inline
bool esGetActiveFlag(const struct entity_storage_active_flag_ptr * ptr)
{
   unsigned char mask = 1 << ptr->bit;
   return ((*ptr->flagByte) & mask) == mask;
}

static inline
void esSetActiveFlag(const struct entity_storage_active_flag_ptr * ptr, 
                     bool value)
{
   unsigned char mask = 1 << ptr->bit;
   if(value)
   {
      (*ptr->flagByte) |= mask;
   }
   else
   {
      (*ptr->flagByte) &= ~mask;
   }
}

static inline
void esIteratorSetAt(const struct entity_storage * storage, 
                     struct entity_storage_iterator * iter,
                     size_t index)
{
   esSetActiveFlagPointer(storage, &iter->activePtr, index);
   iter->index = index;
   iter->traveled = 0;
   iter->storage = storage;
   if(index < storage->maxCount)
   {
      unsigned char * entityData = (unsigned char *) storage->entityData;
      iter->memory = &entityData[index * storage->entitySize];
   }
   else
   {
      iter->memory = NULL;
   }
}

static inline
void esIteratorNext(struct entity_storage_iterator * iter)
{
   iter->index ++;
   if(iter->index < iter->storage->maxCount)
   {
      iter->memory += iter->storage->entitySize;
      iter->activePtr.bit ++;
      if(iter->activePtr.bit >= 8)
      {
         iter->activePtr.bit = 0;
         iter->activePtr.flagByte ++;
      }
   }
   else
   {
      iter->index = 0;
      iter->memory = iter->storage->entityData;
      iter->activePtr.bit = 0;
      iter->activePtr.flagByte = iter->storage->activeFlags;
   }
   iter->traveled ++;
}

static inline
size_t esGetIndexFromPtr(const struct entity_storage * storage,
                         void * ptr)
{
   size_t diff = (size_t)((unsigned char*)ptr - (unsigned char*)storage->entityData);
   return diff / storage->entitySize;
   
}

void esInit(struct entity_storage * storage, 
            size_t entitySize, 
            size_t maxCount, 
            unsigned char * activeFlags, 
            void * entityData)
{
   
   storage->entitySize  = entitySize;
   storage->maxCount    = maxCount;
   storage->activeFlags = activeFlags;
   storage->entityData  = entityData;
   
   esClear(storage);
}


void * esTake(struct entity_storage * storage)
{
   struct entity_storage_iterator iter;
   void * mem;
   esIteratorSetAt(storage, &iter, storage->nextEntityHint);
   while(esGetActiveFlag(&iter.activePtr) && iter.traveled < storage->maxCount)
   {
      esIteratorNext(&iter);
   }
   
   if(esGetActiveFlag(&iter.activePtr))
   {
      // We are full return NULL
      return NULL;
   }
   mem = iter.memory;
   esSetActiveFlag(&iter.activePtr, true);
   
   // Go next one more time to get the hint index
   esIteratorNext(&iter);
   storage->nextEntityHint = iter.index;
   
   memset(mem, 0, storage->entitySize);
   storage->count ++;
   
   return mem;
}

void esFree(struct entity_storage * storage, void * ptr)
{
   size_t index = esGetIndexFromPtr(storage, ptr);
   struct entity_storage_iterator iter;
   esIteratorSetAt(storage, &iter, index);
   if(iter.memory == NULL)
   {
      fprintf(stderr, "Memory 0x%p is not owned by entity storage 0x%p (index: %lu) and so can't be Freed\n", ptr, storage, index); 
   }
   else if(!esGetActiveFlag(&iter.activePtr))
   {
      fprintf(stderr, "Memory 0x%p at index %lu for storage 0x%p cannot be Freed because it's not taken\n", ptr, index, storage);
   }
   else
   {
      esSetActiveFlag(&iter.activePtr, false);
      storage->count --;
   }
}

void * esNext(const struct entity_storage * storage, void * ptr)
{
   struct entity_storage_iterator iter;
   if(ptr == NULL)
   {
      esIteratorSetAt(storage, &iter, 0);
      while(!esGetActiveFlag(&iter.activePtr) &&
            iter.traveled < storage->maxCount)
      {
         esIteratorNext(&iter);
      }
      if(esGetActiveFlag(&iter.activePtr))
      {
         return iter.memory;
      }
   }
   else
   {
      size_t index = esGetIndexFromPtr(storage, ptr);
      esIteratorSetAt(storage, &iter, index);
      esIteratorNext(&iter);
      while(!esGetActiveFlag(&iter.activePtr) &&
            iter.index > index)
      {
         esIteratorNext(&iter);
      }
      
      if(iter.index > index)
      {
         return iter.memory;
      }
   }
   return NULL;
}

void esClear(struct entity_storage * storage)
{
   size_t i;
   size_t activeFlagsSize = esGetActiveFlagsSizeFromMaxCount(storage->maxCount);
   
   storage->nextEntityHint = 0;
   storage->count = 0;
   
   // Zero out all the active flags
   for(i = 0; i < activeFlagsSize; i ++)
   {
      storage->activeFlags[i] = 0;
   }
}
